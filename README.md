# Talleres UNAL

> Esto es un monorepo, la entrga de cada una de las tareas las dejo como tag, acontinuacion asocio el menu con los tag de cada taller.

## Contenido

| Taller | Tag | Folder |
| -- | -- | -- |
| Rero 0 | https://gitlab.com/unal_mobile/talleres/-/tags/reto_0 | Hello World |
| Reto 1 |  https://app.diagrams.net/#G11Yi_nwoGVWRsYN4WAQCPuTkKF0baoDe8 | -- |
| Reto 3 |  https://gitlab.com/unal_mobile/talleres/-/tags/tic-tac-toe | Tic Tac Toe |
| Reto 4 |  https://gitlab.com/unal_mobile/talleres/-/tags/tic-tac-toe_reto_4 | Tic Tac Toe Menus |
| Reto 5 |  https://gitlab.com/unal_mobile/talleres/-/tags/tic-tac-toe_reto_5| Tic Tac Toe Imagenes |
| Reto 6 |  https://gitlab.com/unal_mobile/talleres/-/tags/rotation-landscape-and-session| Tic Tac Toe Imagenes |
| Reto 7 |  https://gitlab.com/unal_mobile/talleres/-/tags/on-line-game| Tic Tac Toe On-Line |
| Reto 8 |  https://gitlab.com/unal_mobile/talleres/-/tags/empresas| Contactos Empresa |
| Reto 9 |  https://gitlab.com/unal_mobile/talleres/-/tags/mapa| Mapa |
| Reto 10 |  https://gitlab.com/unal_mobile/talleres/-/tags/api| API |





