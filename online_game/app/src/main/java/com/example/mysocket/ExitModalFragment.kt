package com.example.mysocket

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlin.system.exitProcess

class ExitModalFragment : BottomSheetDialogFragment(), View.OnClickListener{


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View?{

        val view: View = inflater!!.inflate(R.layout.exit_modal_fragment, container, false)
        val btnExit : Button = view.findViewById(R.id.button_exit)

        btnExit.setOnClickListener(this)
        btnExit.setOnClickListener { view ->
            Log.d("btnSetup", "Selected")
            print("tales de mileto")
            exitProcess(0);

        }

        return view
    }


    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.button_cancel -> {
                print("tales")
            }

            else -> {
            }
        }
    }


}