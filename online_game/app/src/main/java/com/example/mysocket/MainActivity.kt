package com.example.mysocket

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.res.Configuration
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn = findViewById<Button>(R.id.button1)
        val player_txt = findViewById<TextView>(R.id.player_txt)
        val nickname = findViewById<TextView>(R.id.nickname)
        val button1 = findViewById<Button>(R.id.button1)
        val button2 = findViewById<Button>(R.id.button2)
        val button3 = findViewById<Button>(R.id.button3)
        val button4 = findViewById<Button>(R.id.button4)
        val button5 = findViewById<Button>(R.id.button5)
        val button6 = findViewById<Button>(R.id.button6)
        val button7 = findViewById<Button>(R.id.button7)
        val button8 = findViewById<Button>(R.id.button8)
        val button9 = findViewById<Button>(R.id.button9)
        val register = findViewById<Button>(R.id.register)
        val reset = findViewById<Button>(R.id.reset)
        val name = findViewById<TextView>(R.id.name)


        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            println("++++++++++++++++ ROTO ====================")
            
        }



        SocketHandler.setSocket()
        val mSocket = SocketHandler.getSocket()
        mSocket.connect()


        button1.isEnabled = false
        button1.isEnabled = false
        button2.isEnabled = false
        button3.isEnabled = false
        button4.isEnabled = false
        button5.isEnabled = false
        button6.isEnabled = false
        button7.isEnabled = false
        button8.isEnabled = false
        button9.isEnabled = false



        val valueAnimator = ValueAnimator.ofFloat(0f, -40f)

        valueAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            button1.rotationX = value
            button2.rotationX = value
            button3.rotationX = value
            button4.rotationX = value
            button5.rotationX = value
            button6.rotationX = value
            button7.rotationX = value
            button8.rotationX = value
            button9.rotationX = value
        }
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = 1
        valueAnimator.start()

        valueAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            button1.rotationY = value
            button2.rotationY = value
            button3.rotationY = value
            button4.rotationY = value
            button5.rotationY = value
            button6.rotationY = value
            button7.rotationY = value
            button8.rotationY = value
            button9.rotationY = value
        }
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = 1
        valueAnimator.start()

        player_txt.text = "..."

        register.setOnClickListener {
            button1.isEnabled = true
            button2.isEnabled = true
            button3.isEnabled = true
            button4.isEnabled = true
            button5.isEnabled = true
            button6.isEnabled = true
            button7.isEnabled = true
            button8.isEnabled = true
            button9.isEnabled = true
            mSocket.emit("register", "{\"name\":\"${name.text.toString()}\"}")
            player_txt.setBackgroundColor(Color.RED)
            player_txt.setTextColor(Color.WHITE)



            val valueAnimator = ValueAnimator.ofFloat(0f, 0f)

            valueAnimator.addUpdateListener {
                val value = it.animatedValue as Float
                button1.rotationX = value
                button2.rotationX = value
                button3.rotationX = value
                button4.rotationX = value
                button5.rotationX = value
                button6.rotationX = value
                button7.rotationX = value
                button8.rotationX = value
                button9.rotationX = value
            }
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.duration = 1
            valueAnimator.start()

            valueAnimator.addUpdateListener {
                val value = it.animatedValue as Float
                button1.rotationY = value
                button2.rotationY = value
                button3.rotationY = value
                button4.rotationY = value
                button5.rotationY = value
                button6.rotationY = value
                button7.rotationY = value
                button8.rotationY = value
                button9.rotationY = value
            }
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.duration = 1
            valueAnimator.start()

        }

        button1.setOnClickListener {
            button1.isClickable = false
            mSocket.emit("game", "{\"position\":\"0\"}")
        }

        button2.setOnClickListener {
            button2.text = token
            mSocket.emit("game", "{\"position\":\"1\"}")
        }


        button3.setOnClickListener {
            button3.text = token
            mSocket.emit("game", "{\"position\":\"2\"}")
        }

        button4.setOnClickListener {
            button4.text = token
            mSocket.emit("game", "{\"position\":\"3\"}")
        }

        button5.setOnClickListener {
            button5.text = token
            mSocket.emit("game", "{\"position\":\"4\"}")
        }

        button6.setOnClickListener {
            button6.text = token
            mSocket.emit("game", "{\"position\":\"5\"}")

        }

        button7.setOnClickListener {
            button7.text = token
            mSocket.emit("game", "{\"position\":\"6\"}")
        }

        button8.setOnClickListener {
            button8.text = token
            mSocket.emit("game", "{\"position\":\"7\"}")
        }

        button9.setOnClickListener {
            button9.text = token
            mSocket.emit("game", "{\"position\":\"8\"}")
        }

        reset.setOnClickListener {
            button1.isEnabled = false
            button1.isEnabled = false
            button2.isEnabled = false
            button3.isEnabled = false
            button4.isEnabled = false
            button5.isEnabled = false
            button6.isEnabled = false
            button7.isEnabled = false
            button8.isEnabled = false
            button9.isEnabled = false

            button1.text = ""
            button1.text = ""
            button2.text = ""
            button3.text = ""
            button4.text = ""
            button5.text = ""
            button6.text = ""
            button7.text = ""
            button8.text = ""
            button9.text = ""
            mSocket.emit("reset", "1")


            val valueAnimator = ValueAnimator.ofFloat(0f, 40f)

            valueAnimator.addUpdateListener {
                val value = it.animatedValue as Float
                button1.rotationX = value
                button2.rotationX = value
                button3.rotationX = value
                button4.rotationX = value
                button5.rotationX = value
                button6.rotationX = value
                button7.rotationX = value
                button8.rotationX = value
                button9.rotationX = value
            }
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.duration = 1
            valueAnimator.start()

            valueAnimator.addUpdateListener {
                val value = it.animatedValue as Float
                button1.rotationY = value
                button2.rotationY = value
                button3.rotationY = value
                button4.rotationY = value
                button5.rotationY = value
                button6.rotationY = value
                button7.rotationY = value
                button8.rotationY = value
                button9.rotationY = value
            }
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.duration = 1
            valueAnimator.start()
        }



        mSocket.on("move") { args ->
            if (args[0] != null) {
                val counter = args[0] as String
                var data = ""
                var data_position = ""
                var data_token = ""
                runOnUiThread {
                    data = counter.toString()
                    data_token = data.split("-")[1]
                    data_position = data.split("-")[0]
                    changeButtonRemote(data_position, data_token)
                }
            }
        }


        mSocket.on("data_game") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as String
                println(counter)
                runOnUiThread {
                    player_txt.text = counter.toString()
                    player_txt.setBackgroundColor(Color.BLACK)
                    player_txt.setTextColor(Color.WHITE)

                }
            }
        }


        mSocket.on("winner") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as String
                runOnUiThread {
                    println("=========")
                    println(counter.toString())
                    println("=========")
                    player_txt.text = " ** ${counter.toString()} is the WINNER!!! **"
                    player_txt.setBackgroundColor(Color.YELLOW)
                    player_txt.setTextColor(Color.BLACK)
                    nickname.setTextColor(Color.BLACK)

                }
            }
        }


        mSocket.on("info_server") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as String
                println(counter)
                runOnUiThread {

                    if (counter != "") {
                        player_txt.text = "Game Server ONLINE :)"
                        player_txt.setBackgroundColor(Color.BLUE)
                        player_txt.setTextColor(Color.WHITE)
                    } else {
                        player_txt.text = "Game Server OFFLINE :("
                        player_txt.setBackgroundColor(Color.RED)
                        player_txt.setTextColor(Color.WHITE)

                    }

                }
            }
        }

        mSocket.on("info_server_filed") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as String
                println(counter)
                runOnUiThread {
                    player_txt.text = "Game Server OFFLINE :("
                    player_txt.setBackgroundColor(Color.RED)
                    player_txt.setTextColor(Color.WHITE)
                    button1.isEnabled = false
                    button1.isEnabled = false
                    button2.isEnabled = false
                    button3.isEnabled = false
                    button4.isEnabled = false
                    button5.isEnabled = false
                    button6.isEnabled = false
                    button7.isEnabled = false
                    button8.isEnabled = false
                    button9.isEnabled = false

                    button1.text = ""
                    button1.text = ""
                    button2.text = ""
                    button3.text = ""
                    button4.text = ""
                    button5.text = ""
                    button6.text = ""
                    button7.text = ""
                    button8.text = ""
                    button9.text = ""



                    val valueAnimator = ValueAnimator.ofFloat(0f, -40f)

                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        button1.rotationX = value
                        button2.rotationX = value
                        button3.rotationX = value
                        button4.rotationX = value
                        button5.rotationX = value
                        button6.rotationX = value
                        button7.rotationX = value
                        button8.rotationX = value
                        button9.rotationX = value
                    }
                    valueAnimator.interpolator = LinearInterpolator()
                    valueAnimator.duration = 1
                    valueAnimator.start()

                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        button1.rotationY = value
                        button2.rotationY = value
                        button3.rotationY = value
                        button4.rotationY = value
                        button5.rotationY = value
                        button6.rotationY = value
                        button7.rotationY = value
                        button8.rotationY = value
                        button9.rotationY = value
                    }
                    valueAnimator.interpolator = LinearInterpolator()
                    valueAnimator.duration = 1
                    valueAnimator.start()

                }
            }
        }

        mSocket.on("missing") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as String
                runOnUiThread {
                    player_txt.text = counter.toString()
                    player_txt.setBackgroundColor(Color.RED)
                    player_txt.setTextColor(Color.WHITE)

                }
            }
        }

        mSocket.on("token_data") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as String
                println(counter)
                runOnUiThread {
                    println(counter.toString())

                }
            }
        }

        mSocket.on("animate") { args ->
            println(args[0])
            if (args[0] != null) {
                val counter = args[0] as Int
                println(counter)
                runOnUiThread {

                    button1.setTextColor(Color.YELLOW)
                    button1.setTextColor(Color.YELLOW)
                    button2.setTextColor(Color.YELLOW)
                    button3.setTextColor(Color.YELLOW)
                    button4.setTextColor(Color.YELLOW)
                    button5.setTextColor(Color.YELLOW)
                    button6.setTextColor(Color.YELLOW)
                    button7.setTextColor(Color.YELLOW)
                    button8.setTextColor(Color.YELLOW)
                    button9.setTextColor(Color.YELLOW)

                    val valueAnimator = ValueAnimator.ofFloat(0f, -40f)

                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        button1.rotationX = value
                        button2.rotationX = value
                        button3.rotationX = value
                        button4.rotationX = value
                        button5.rotationX = value
                        button6.rotationX = value
                        button7.rotationX = value
                        button8.rotationX = value
                        button9.rotationX = value
                    }
                    valueAnimator.interpolator = LinearInterpolator()
                    valueAnimator.duration = 1
                    valueAnimator.start()

                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        button1.rotationY = value
                        button2.rotationY = value
                        button3.rotationY = value
                        button4.rotationY = value
                        button5.rotationY = value
                        button6.rotationY = value
                        button7.rotationY = value
                        button8.rotationY = value
                        button9.rotationY = value
                    }
                    valueAnimator.interpolator = LinearInterpolator()
                    valueAnimator.duration = 1
                    valueAnimator.start()



                }
            }
        }



        mSocket.on("reset") { args ->
            if (args[0] != null) {
                val counter = args[0] as Int
                runOnUiThread {
                    button1.isEnabled = false
                    button1.isEnabled = false
                    button2.isEnabled = false
                    button3.isEnabled = false
                    button4.isEnabled = false
                    button5.isEnabled = false
                    button6.isEnabled = false
                    button7.isEnabled = false
                    button8.isEnabled = false
                    button9.isEnabled = false

                    button1.text = ""
                    button1.text = ""
                    button2.text = ""
                    button3.text = ""
                    button4.text = ""
                    button5.text = ""
                    button6.text = ""
                    button7.text = ""
                    button8.text = ""
                    button9.text = ""

                    player_txt.text = "..."
                    player_txt.setBackgroundColor(Color.BLUE)
                    player_txt.setTextColor(Color.WHITE)



                    val valueAnimator = ValueAnimator.ofFloat(0f, -40f)

                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        button1.rotationX = value
                        button2.rotationX = value
                        button3.rotationX = value
                        button4.rotationX = value
                        button5.rotationX = value
                        button6.rotationX = value
                        button7.rotationX = value
                        button8.rotationX = value
                        button9.rotationX = value
                    }
                    valueAnimator.interpolator = LinearInterpolator()
                    valueAnimator.duration = 1
                    valueAnimator.start()

                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        button1.rotationY = value
                        button2.rotationY = value
                        button3.rotationY = value
                        button4.rotationY = value
                        button5.rotationY = value
                        button6.rotationY = value
                        button7.rotationY = value
                        button8.rotationY = value
                        button9.rotationY = value
                    }
                    valueAnimator.interpolator = LinearInterpolator()
                    valueAnimator.duration = 1
                    valueAnimator.start()

                }
            }
        }


    }

    fun changeButton(btn: Button, txt: String) {
        btn.text = txt
    }

    fun changeButtonRemote(position: String, token: String) {

        println("====2======")
        println(position)

        val button1 = findViewById<Button>(R.id.button1)
        val button2 = findViewById<Button>(R.id.button2)
        val button3 = findViewById<Button>(R.id.button3)
        val button4 = findViewById<Button>(R.id.button4)
        val button5 = findViewById<Button>(R.id.button5)
        val button6 = findViewById<Button>(R.id.button6)
        val button7 = findViewById<Button>(R.id.button7)
        val button8 = findViewById<Button>(R.id.button8)
        val button9 = findViewById<Button>(R.id.button9)

        if (position == "0") {
            button1.text = token
        }
        if (position == "1") {
            button2.text = token
        }
        if (position == "2") {
            button3.text = token
        }
        if (position == "3") {
            button4.text = token
        }
        if (position == "4") {
            button5.text = token
        }
        if (position == "5") {
            button6.text = token
        }
        if (position == "6") {
            button7.text = token
        }
        if (position == "7") {
            button8.text = token
        }
        if (position == "8") {
            button9.text = token
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        // Checks the orientation of the screen
        if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show()
        } else if (newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val gameModalFragment = GameServerFragment()
        val infoModalFragment = InfoModalFragment()
        val levelModalFragment = LevelModalFragment()
        val exitModalFragment = ExitModalFragment()
        val resetModalFragment = ResetModalFragment()



        when (item.itemId) {
            R.id.item_level -> gameModalFragment.show(supportFragmentManager, "ServerModalFragment")
            R.id.nav_info -> infoModalFragment.show(supportFragmentManager, "InfoModalFragment")
            R.id.item_level -> levelModalFragment.show(supportFragmentManager, "LevelModalFragment")
            R.id.item_exit -> exitModalFragment.show(supportFragmentManager, "ExitModalFragment")
            R.id.item_refresh -> resetModalFragment.show(
                supportFragmentManager,
                "ResetModalFragment"
            )

        }
        true
        return super.onOptionsItemSelected(item)
    }

}