package com.homework.tic_tac_toe

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlin.random.Random

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var button1: Button
    lateinit var button2: Button
    lateinit var button3: Button
    lateinit var button4: Button
    lateinit var button5: Button
    lateinit var button6: Button
    lateinit var button7: Button
    lateinit var button8: Button
    lateinit var button9: Button
    lateinit var button10: Button
//    lateinit var button_cancel: Button
//    lateinit var button_exit: Button
    lateinit var btn_navigation: com.google.android.material.bottomnavigation.BottomNavigationView
    lateinit var win: TextView
    lateinit var mp: TextView
    lateinit var mm: TextView
    lateinit var positionSelectedMachine: ArrayList<String>
    lateinit var positionSelectedPlayer: ArrayList<String>
    lateinit var moves: ArrayList<Int>
    lateinit var susses: ArrayList<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        positionSelectedMachine = ArrayList()
        positionSelectedPlayer = ArrayList()
        moves = ArrayList()
        susses = ArrayList()
        moves.addAll(listOf(0, 1, 2, 3, 4, 5, 6, 7, 8))
        susses.addAll(listOf("012", "345", "678", "048", "246", "036", "147", "258"
            , "210", "543", "876", "840", "642", "630", "741", "852", "354"))

        win = findViewById(R.id.win)
        mp = findViewById(R.id.mp)
        mm = findViewById(R.id.mm)
        button1 = findViewById(R.id.button1)
        button2 = findViewById(R.id.button2)
        button3 = findViewById(R.id.button3)
        button4 = findViewById(R.id.button4)
        button5 = findViewById(R.id.button5)
        button6 = findViewById(R.id.button6)
        button7 = findViewById(R.id.button7)
        button8 = findViewById(R.id.button8)
        button9 = findViewById(R.id.button9)
        button10 = findViewById(R.id.button10)

        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        button10.setOnClickListener(this)



    }

    override fun onClick(v: View?) {


        val buttons =
            arrayOf(button1, button2, button3, button4, button5, button6, button7, button8, button9)
        var btnClicked = findViewById<Button>(v!!.id)
        var machineMove = 0
        var selected = btnClicked.tag.toString().toInt()
        btnClicked.isClickable = false
        btnClicked.setText("O")


        println(btnClicked.tag.toString().toInt())
        println(btnClicked.tag.toString())


        if (btnClicked.tag.toString().toInt() != 20) {
            btnClicked.setBackgroundResource(R.drawable.bug_circulo)
            moves.remove(selected)

            if (moves.count() != 0) {
                machineMove = Random.nextInt(0, moves.count())
                println("maquina selecciono indice:")
                println(machineMove)
                println("maquina selecciono valor:")
                println(machineMove)
                buttons.get(moves[machineMove]).setText("X")

                buttons.get(moves[machineMove]).setBackgroundResource(R.drawable.person_cruz)

                buttons.get(moves[machineMove]).isClickable = false
                positionSelectedMachine.add(
                    buttons.get(moves[machineMove]).tag.toString().toString()
                )
                println("maquina Borra:")
                println(machineMove)
                moves.removeAt(machineMove)
                println("array despues de maquina:")
                println(moves)
            }

            if (moves.count() == 0) {
                println("Juego terminado!!!")
                println(moves.count())

            }

            positionSelectedPlayer.add(selected.toString())

            mp.setText("   Moves Player: " + positionSelectedMachine.joinToString())
            mm.setText("   Moves Machine: " + positionSelectedMachine.joinToString())

            if (positionSelectedPlayer.size >= 3) {
                println("Validating machine move....")
                println()
                for (i in susses) {
                    println("======")
                    println(i)
                    println(positionSelectedPlayer.joinToString().indexOf(i))
                    println(positionSelectedPlayer.joinToString().replace(",", "").replace(" ", "").contains(i, ignoreCase = true))
                    println(positionSelectedPlayer.joinToString().replace(",", "").replace(" ", ""))
                    if (positionSelectedPlayer.joinToString().replace(",", "").replace(" ", "").contains(i, ignoreCase = true)) {
                        println("=== PLAYER WIN! ===")
                        println(positionSelectedPlayer.joinToString().indexOf(i))

                        for (item in positionSelectedPlayer){
                            buttons[item.toInt()].setBackgroundColor(Color.RED)
                            buttons[item.toInt()].setBackgroundResource(R.drawable.stars)
                        }

                        win.setText("=== PLAYER WIN! ===")
                        break;
                    }

                }

            }

            if (positionSelectedMachine.size >= 3) {
                println("Validating machine move....")
                println()
                for (i in susses) {
                    println("======")
                    println(i)
                    println("===***===")
                    if (positionSelectedMachine.joinToString().replace(",", "").replace(" ", "").contains(i, ignoreCase = true)) {
                        println("=== MACHINE WIN! ===")
                        println(positionSelectedPlayer.joinToString().indexOf(i))
                        win.setText("=== MACHINE WIN! ===")
                        break;
                    }

                }
            }

        }else{
            println("reiniciando......")
            moves.addAll(listOf())
            moves.addAll(listOf(0, 1, 2, 3, 4, 5, 6, 7, 8))
            susses.addAll(listOf("012", "345", "678", "048", "246", "036", "147", "258"
                , "210", "543", "876", "840", "642", "630", "741", "852", "354"))
            positionSelectedMachine = ArrayList()
            positionSelectedPlayer = ArrayList()
            win.setText("")
            button1.setText("")
            button2.setText("")
            button3.setText("")
            button4.setText("")
            button5.setText("")
            button6.setText("")
            button7.setText("")
            button8.setText("")
            button9.setText("")

            btnClicked.isClickable = true

            button1.isClickable = true
            button2.isClickable = true
            button3.isClickable = true
            button4.isClickable = true
            button5.isClickable = true
            button6.isClickable = true
            button7.isClickable = true
            button8.isClickable = true
            button9.isClickable = true
            button10.isClickable = true

            button1.setBackgroundColor(Color.BLUE)
            button2.setBackgroundColor(Color.BLUE)
            button3.setBackgroundColor(Color.BLUE)
            button4.setBackgroundColor(Color.BLUE)
            button5.setBackgroundColor(Color.BLUE)
            button6.setBackgroundColor(Color.BLUE)
            button7.setBackgroundColor(Color.BLUE)
            button8.setBackgroundColor(Color.BLUE)
            button9.setBackgroundColor(Color.BLUE)
            button10.isClickable = true

            button1.isActivated = true
            button2.isActivated = true
            button3.isActivated = true
            button4.isActivated = true
            button5.isActivated = true
            button6.isActivated = true
            button7.isActivated = true
            button8.isActivated = true
            button9.isActivated = true
            button10.isActivated = true

            button10.setText("Reiniciar el Juego")
            mp.setText("")
            mm.setText("")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val infoModalFragment = InfoModalFragment()
        val levelModalFragment = LevelModalFragment()
        val exitModalFragment = ExitModalFragment()
        val resetModalFragment = ResetModalFragment()

        when(item.itemId){
            R.id.nav_info -> infoModalFragment.show(supportFragmentManager, "InfoModalFragment")
            R.id.item_level -> levelModalFragment.show(supportFragmentManager, "LevelModalFragment")
            R.id.item_exit -> exitModalFragment.show(supportFragmentManager, "ExitModalFragment")
            R.id.item_refresh -> resetModalFragment.show(supportFragmentManager, "ResetModalFragment")
            R.id.item_refresh -> tales()
        }
        true
        return super.onOptionsItemSelected(item)
    }

    fun tales(): String{
        print("sdsds")
        return "tales"
    }


}